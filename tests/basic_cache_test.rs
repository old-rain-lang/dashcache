use dashcache::{DashCache, GlobalCache};
use elysees::Arc;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref GLOBAL_CACHE: DashCache<Arc<usize>> = DashCache::new();
}

#[test]
fn basic_functionality() {
    let mut arcs: Vec<_> = (0..20).map(Arc::new).collect();
    // Check caching
    for arc in arcs.iter() {
        assert_eq!(GLOBAL_CACHE.cache(arc.clone()), *arc);
    }
    let dup_arcs: Vec<_> = (0..20).map(Arc::new).collect();
    // Check deduplication
    for (arc, dup_arc) in arcs.iter().zip(dup_arcs.iter()) {
        assert_eq!(GLOBAL_CACHE.cache(dup_arc.clone()), *arc);
    }
    // Check GC fails for referenced arcs
    for arc in arcs.iter() {
        assert!(GLOBAL_CACHE.try_gc(&mut arc.clone()).is_none());
    }
    // Check GC succeeds for unreferenced arcs
    for mut arc in arcs.drain(..) {
        assert!(GLOBAL_CACHE.try_gc(&mut arc).is_some())
    }
}
