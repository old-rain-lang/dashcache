use criterion::{black_box, criterion_group, criterion_main, Criterion};
use dashcache::{DashCache, GlobalCache};
use lazy_static::lazy_static;
use rand::{thread_rng, Rng};

lazy_static! {
    pub static ref ELYSEES_GLOBAL_CACHE: DashCache<elysees::Arc<usize>> = DashCache::new();
    pub static ref STD_GLOBAL_CACHE: DashCache<std::sync::Arc<usize>> = DashCache::new();
}

pub fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("cache elysees", |b| {
        let mut rng = thread_rng();
        let gen: usize = rng.gen();
        b.iter(|| black_box(ELYSEES_GLOBAL_CACHE.cache(gen)))
    });
    c.bench_function("cache std", |b| {
        let mut rng = thread_rng();
        let gen: usize = rng.gen();
        b.iter(|| black_box(STD_GLOBAL_CACHE.cache(gen)))
    });
    let local_cache = DashCache::<std::rc::Rc<usize>>::new();
    c.bench_function("local cache std", move |b| {
        let mut rng = thread_rng();
        let gen: usize = rng.gen();
        b.iter(|| black_box(local_cache.cache(gen)))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
