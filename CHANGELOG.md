# 0.2.2

- Added `try_gc_global` method to `GlobalCache` trait

# 0.2.1

- Added `Default` implementation for `DashCache`

# 0.2.0

- Rewrote crate to use `GlobalCache` and `Caches` traits

# 0.1.0

- Initial release